if Code.ensure_loaded?(Scrivener) do
  defmodule JaSerializer.Builder.ScrivenerLinks do
    @moduledoc """
    Builds JSON-API spec pagination links for %Scrivener.Page{}.
    """

    @spec build(map) :: map
    def build(%{data: data = %Scrivener.Page{}, opts: opts, conn: conn}) do
      data = %{
        number: Map.get(data, scrivener_page_number_key(), data.page_number),
        size: Map.get(data, scrivener_page_size_key(), data.page_size),
        total: data.total_pages,
        base_url: opts[:base_url]
      }

      JaSerializer.Builder.PaginationLinks.build(data, conn)
    end

    def scrivener_page_number_key(), do: get_env(:ja_serializer, [:scrivener, :page_number_key])
    def scrivener_page_size_key(), do: get_env(:ja_serializer, [:scrivener, :page_size_key])

    defp get_env(config, [ first_key | keys ]) do
      Enum.reduce(keys, get_env(config, first_key), &get_env(&2, &1))
    end

    defp get_env(config, key) when is_atom(config) do
      config
        |> Application.get_env(key)
        |> get_value()
    end

    defp get_env(config, key) when is_map(config) do
      config
        |> Map.get(key)
        |> get_value()
    end

    defp get_value({:system, var, default}), do: System.get_env(var) || default
    defp get_value({:system, var}), do: System.get_env(var)
    defp get_value(value) when is_binary(value), do: String.to_atom(value)
    defp get_value(value), do: value
  end
end
